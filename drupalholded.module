<?php

/**
 * @file
 * Iguana API integration module file.
 */
 
use Drupal\Core\Database\Database;
use Drupal\holded\HoldedConnection;
 
/**
 * Implements hook_cron().
 */
function drupalholded_cron() {
  $state     = \Drupal::state();
  $locked    = $state->get('drupalholded.items_import_semaphore', FALSE);
  $last_time = $state->get('drupalholded.items_import_last', FALSE);
 
  if (!$locked && $last_time) {
    $database   = Database::getConnection();
    $holded     = new HoldedConnection();
    $queue      = \Drupal::queue('holded_items_import_worker');
    $api_limit  = $state->get('drupalholded.cron_download_limit', 100);
    $save_limit = $state->get('drupalholded.cron_process_limit', 10);
    $data       = NULL;
    $new_data   = [];
 
    // Pull all data into an array
    // TODO: limit checks in case all of the thousands of Teas have new
    // revisions
    do {
      // If there is have a 'next' URL returned, use that one for simplicity
      $next_page = NULL;
      if (isset($data->pagination->next)) {
        $next_page = $data->pagination->next;
      }
      $data = $holded->queryEndpoint('teasDetailFull', [
        'limit'      => $api_limit,
        'start_time' => $last_time,
        'next_page'  => isset($next_page) ? $next_page : NULL,
      ]);
      $new_data = array_merge($new_data, $data->response_data);
    } while (isset($data->pagination->next));
 
    $hids      = [];
    $new_count = count($new_data);
    foreach ($new_data as $index => $items_data) {
      if (empty($items_data->hid)) {
        \Drupal::logger('drupalholded')->warning(t('Empty HID at progress @p for the data:<br /><pre>@v</pre>', [
          '@v' => print_r($items_data, TRUE),
          '@p' => $index,
        ]));
        continue;
      }
      elseif (!is_numeric($items_data->hid)) {
        \Drupal::logger('drupalholded')->warning(t('Non-numeric HID at progress @p for the data:<br /><pre>@v</pre>', [
          '@v' => print_r($holded_data, TRUE),
          '@p' => $index,
        ]));
        continue;
      }
      // Save the data to the local database
      $database->merge('drupalholded_items_staging')
        ->key(['hid' => (int) $items_data->hid])
        ->insertFields([
          'hid'  => (int) $items_data->hid,
          'data' => serialize($items_data),
        ])
        ->updateFields(['data' => serialize($items_data)])
        ->execute()
      ;
      $hids[] = (int) $items_data->hid;
      // If enough Teas have been stored or the last one just was strored,
      // then queue up a worker to process them and reset the IDs array
      if (count($hids) == $save_limit || $index + 1 == $new_count) {
        $queue->createItem(['hids' => $hids]);
        $hids = [];
      }
    }
    // Store the timestamp in state
    $last_time = \Drupal::time()->getRequestTime();
    \Drupal::state()->set('drupalholded.items_import_last', $last_time);
  }
  elseif ($locked) {
    \Drupal::logger('drupalholded')->warning(t('Drupal Holded Cron did not run because it is locked.'));
  }
}